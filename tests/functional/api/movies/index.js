import chai from "chai";
import request from "supertest";
import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import api from "../../../../index";
import Movie from "../../../../api/movies/movieModel";
import User from "../../../../api/users/userModel";
import movies from "../../../../seedData/movies";

const expect = chai.expect;

describe("Movies endpoint", () => {
  let token; // Store the token for later use
  
  before(async () => {
    // Connect to the database
    await mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    // Create a test user and get the token
    const testUser = new User({
      username: "testuser",
      password: "test123@",
    });

    await testUser.save();

    token = jwt.sign({ username: "testuser" }, process.env.SECRET, {
      expiresIn: "1h",
    });
  });

  after(async () => {
    // Drop the database after all tests are done
    await mongoose.connection.db.dropDatabase();
    await mongoose.disconnect();
    api.close();
  });

  beforeEach(async () => {
    // Clear the Movie collection and insert seed data
    await Movie.deleteMany();
    await Movie.collection.insertMany(movies);
  });

  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("GET /api/movies", () => {
    it("should return 20 movies and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
    it("should return an error without authentication", (done) => {
      request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .expect(401) // Expecting Unauthorized status
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  describe("GET /api/movies/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie when authenticated", async () => {
        const res = await request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", `Bearer ${token}`)
          .expect(200);
        expect(res.body).to.have.property("title", movies[0].title);
      });
  
      it("should return the matching movie without authentication", async () => {
        const res = await request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .expect(401);
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
      });
    });
  
    describe("when the id is invalid", () => {
      it("should return the NOT found message when authenticated", async () => {
        await request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .set("Authorization", `Bearer ${token}`)
          .expect(404)
          .expect({
            message: "The movie you requested could not be found.",
            status_code: 404,
          });
      });
  
      it("should return the NOT found message without authentication", async () => {
        const res = await request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .expect(401);
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
      });
    });
  });

  describe("GET /api/movies/tmdb/movieList", () => {
    it("should return 20 movies from TMDB and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/movies/tmdb/movieList")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
    it("should return 20 movies from TMDB and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/movies/tmdb/movieList")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/upcoming", () => {
    it("should return 20 upcoming movies and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/movies/tmdb/upcoming")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
    it("should return 20 upcoming movies and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/movies/tmdb/upcoming")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/nowplaying", () => {
    it("should return 20 nowplaying movies and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/movies/tmdb/nowplaying")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
    it("should return 20 nowplaying movies and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/movies/tmdb/nowplaying")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });


  describe("GET /api/movies/tmdb/topRated", () => {
    it("should return 20 topRated movies and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/movies/tmdb/topRated")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
    it("should return 20 topRated movies and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/movies/tmdb/topRated")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  describe("GET /api/movies/tmdb/genres", () => {
    it("should return 20 movies genres and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/movies/tmdb/genres")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.genres).to.be.a("array");
          expect(res.body.genres.length).to.equal(19);
          done();
        });
    });
    it("should return 20 movies genres and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/movies/tmdb/genres")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  

  describe("GET /api/movies/tmdb/:id/movieImages", () => {
    it("should return a list of movie's images and a status 200 when authenticated", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/${movies[0].id}/movieImages`)
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200);
      expect(res.body.backdrops).to.be.an("array").that.is.not.empty;
    });
    it("should return a list of movie's images and a status 200 without authentication", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/${movies[0].id}/movieImages`)
        .set("Accept", "application/json")
        .expect(401);
        expect(res.body).to.have.property("error");
        expect(res.body.error).to.equal("No authorization header");
    });
  });
  
  describe("GET /api/movies/tmdb/:id/movieReviews", () => {
    it("should return a list of movie's reviews and a status 200 when authenticated", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/${movies[0].id}/movieReviews`)
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200);
      expect(res.body.results).to.be.a("array");
    });
    it("should return a list of movie's reviews and a status 200 without authentication", async () => {
      const res = await request(api)
        .get(`/api/movies/tmdb/${movies[0].id}/movieReviews`)
        .set("Accept", "application/json")
        .expect(401);
        expect(res.body).to.have.property("error");
        expect(res.body.error).to.equal("No authorization header");
    });
  });
});
