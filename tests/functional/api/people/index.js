import chai from "chai";
import request from "supertest";
import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import api from "../../../../index";
import People from "../../../../api/people/peopleModel";
import User from "../../../../api/users/userModel";
import people from "../../../../seedData/people";

const expect = chai.expect;

describe("People endpoint", () => {
  let token; // Store the token for later use

  before(async () => {
    // Connect to the database
    await mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    // Create a test user and get the token
    const testUser = new User({
      username: "testuser",
      password: "test123@",
    });

    await testUser.save();

    token = jwt.sign({ username: "testuser" }, process.env.SECRET, {
      expiresIn: "1h",
    });
  });

  after(async () => {
    // Drop the database after all tests are done
    await mongoose.connection.db.dropDatabase();
    await mongoose.disconnect();
    api.close();
  });

  beforeEach(async () => {
    await People.deleteMany();
    await People.collection.insertMany(people);
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("GET /api/people", () => {
    it("should return 20 people and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/people")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          done();
        });
    });
    it("should return 20 people and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/people")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  describe("GET /api/people/tmdb/peopleList/:page", () => {
    it("should return a page with 20 people from TMDB and a status 200 when authenticated", (done) => {
      request(api)
        .get("/api/people/tmdb/peopleList/1")
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
          expect(res.body.page).to.exist;
          expect(res.body.page).to.equal(1);
          done();
        });
    });
    it("should return a page with 20 people from TMDB and a status 200 without authentication", (done) => {
      request(api)
        .get("/api/people/tmdb/peopleList/1")
        .set("Accept", "application/json")
        .expect(401)
        .end((err, res) => {
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
          done();
        });
    });
  });

  describe("GET /api/people/tmdb/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching people when authenticated", async () => {
        const res = await request(api)
          .get(`/api/people/tmdb/${people[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", `Bearer ${token}`)
          .expect(200);
        expect(res.body).to.have.property("name", people[0].name);
      });
      it("should return the matching people without authentication", async () => {
        const res = await request(api)
          .get(`/api/people/tmdb/${people[0].id}`)
          .set("Accept", "application/json")
          .expect(401);
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message when authenticated", () => {
        return request(api)
          .get("/api/people/tmdb/9999999")
          .set("Accept", "application/json")
          .set("Authorization", `Bearer ${token}`)
          .expect(500);
      });
      it("should return the NOT found message without authentication", async () => {
        const res = await request(api)
          .get("/api/people/tmdb/9999999")
          .set("Accept", "application/json")
          .expect(401);
          expect(res.body).to.have.property("error");
          expect(res.body.error).to.equal("No authorization header");
      });
    });
  });

  describe("GET /api/people/tmdb/:id/peopleMovieCredits", () => {
    it("should return a list of person's movie credits and a status 200 when authenticated", async () => {
      const res = await request(api)
        .get(`/api/people/tmdb/${people[0].id}/peopleMovieCredits`)
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200);
      expect(res.body.cast).to.be.an("array").that.is.not.empty;
    });
    it("should return a list of person's movie credits and a status 200 without authentication", async () => {
      const res = await request(api)
        .get(`/api/people/tmdb/${people[0].id}/peopleMovieCredits`)
        .set("Accept", "application/json")
        .expect(401);
        expect(res.body).to.have.property("error");
        expect(res.body.error).to.equal("No authorization header");
    });
  });

  describe("GET /api/people/tmdb/:id/peopleImages", () => {
    it("should return a list of person's images and a status 200 when authenticated", async () => {
      const res = await request(api)
        .get(`/api/people/tmdb/${people[0].id}/peopleImages`)
        .set("Accept", "application/json")
        .set("Authorization", `Bearer ${token}`)
        .expect(200);
      expect(res.body.profiles).to.be.an("array").that.is.not.empty;
    });
    it("should return a list of person's images and a status 200 without authentication", async () => {
      const res = await request(api)
        .get(`/api/people/tmdb/${people[0].id}/peopleImages`)
        .set("Accept", "application/json")
        .expect(401);
        expect(res.body).to.have.property("error");
        expect(res.body.error).to.equal("No authorization header");
    });
  });

});

