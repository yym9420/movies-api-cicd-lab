import chai from "chai";
import request from "supertest";
import mongoose from "mongoose";
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;

describe("Users endpoint", () => {
  
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    await User.deleteMany();
  });

  after(async () => {
    await mongoose.connection.db.dropDatabase();
  });

  beforeEach(async () => {
    await User.deleteMany();

    
    await request(api)
      .post("/api/users?action=register")
      .send({ username: "user1", password: "test123@" });

    await request(api)
      .post("/api/users?action=register")
      .send({ username: "user2", password: "test123@" });
  });

  afterEach(() => {
    api.close();
  });

  describe("GET /api/users", () => {
    it("should return the list of users and a status 200", (done) => {
      request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.an("array");
          expect(res.body.length).to.equal(2);
          done();
        });
    });
  });

  describe("POST /api/users", () => {
    describe("Register action", () => {
      it("should create a new user and return a 201 status", (done) => {
        request(api)
          .post("/api/users?action=register")
          .send({ username: "newuser", password: "test123@" })
          .expect(201)
          .end((err, res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal("User successfully created.");
            done();
          });
      });

      it("should return a 401 status if the username already exists", (done) => {
        request(api)
          .post("/api/users?action=register")
          .send({ username: "user1", password: "test123@" })
          .expect(401)
          .end((err, res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal("User existed!");
            done();
          });
      });
    });

    describe("Authenticate action", () => {
      it("should return a 200 status and a token for valid credentials", (done) => {
        request(api)
          .post("/api/users?action=authenticate")
          .send({ username: "user1", password: "test123@" })
          .expect(200)
          .end((err, res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.token).to.be.a("string");
            done();
          });
      });

      it("should return a 401 status for invalid credentials", (done) => {
        request(api)
          .post("/api/users?action=authenticate")
          .send({ username: "user1", password: "wrongpassword" })
          .expect(401)
          .end((err, res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal("Wrong password.");
            done();
          });
      });
    });

    describe("GET /api/users/:username/movies", () => {
      it("should return the list of favorite movies for the specified user", (done) => {
        request(api)
          .get("/api/users/user1/movies")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.be.a("array");
            done();
          });
      });
    });

    describe("GET /api/users/:username/mustWatch", () => {
      it("should return the list of must-watch movies for the specified user", (done) => {
        request(api)
          .get("/api/users/user1/mustWatch")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.be.a("array");
            done();
          });
      }); 
    });

    describe("POST /api/users/movies", () => {
      it("should add a movie to the user's favorite list", (done) => {
        request(api)
          .post("/api/users/movies")
          .send({ username: "testuser", movieId: "12345" })
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.have.property("message");
            expect(res.body.message).to.equal("Favourite movie added successfully");
            done();
          });
      });
    });


    describe("POST /api/users/mustWatch", () => {
      it("should add a movie to the user's must-watch list", (done) => {
        request(api)
          .post("/api/users/mustWatch")
          .send({ username: "testuser", movieId: "54321" })
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.have.property("message");
            expect(res.body.message).to.equal("MustWatch movie added successfully");
            done();
          });
      });
    });

    describe("DELETE /api/users/movies", () => {
      it("should remove a movie from the user's favorite list", (done) => {
        request(api)
          .delete("/api/users/movies")
          .send({ username: "testuser", movieId: "12345" })
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.have.property("message");
            expect(res.body.message).to.equal("Favourite movie removed successfully");
            done();
          });
      });
    });

    describe("DELETE /api/users/mustWatch", () => {
      it("should remove a movie from the user's must-watch list", (done) => {
        request(api)
          .delete("/api/users/mustWatch")
          .send({ username: "testuser", movieId: "54321" })
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.have.property("message");
            expect(res.body.message).to.equal("MustWatch movie removed successfully");
            done();
          });
      });
    });

  });
});

