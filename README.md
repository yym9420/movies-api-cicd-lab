# Assignment 2 - Agile Software Practice.

Name: YimingYu

## API endpoints.

[List the Web API's endpoints and state the purpose of each one. Indicate those that require authentication.]

+ /api/movies | GET | Gets a list of movies 
+ /api/movies/tmdb/{movieid} | GET | Gets a single movie 
+ /api/movies/tmdb/upcoming | GET | Gets a list of upcoming movies
+ /api/movies/tmdb/nowplaying | GET | Gets a list of upcoming movies
+ /api/movies/tmdb/topRated | GET | Gets a list of topRated movies
+ /api/movies/tmdb/genres | GET | Gets genres of movies
+ /api/movies//tmdb/{movieid}/movieImages | GET | Gets a list of images with the movie
+ /api/movies/tmdb/{movieid}/movieReviews | GET | Gets a list of reviews with the movie
+ /api/people/tmdb/peopleList/{page} | GET | Gets a list of actors and actresses
+ /api/people/tmdb/{peopleId} | GET | Gets a single actor of actress
+ /api/people/tmdb/{peopleId}/peopleMovieCredits | GET | Gets a list of movies credits with the actor of actress
+ /api/people/tmdb/{peopleId}/peopleImages | GET | Gets a list of images with the actor or actress
+ /api/users | GET | Gets all users
+ /api/users | POST | Register(Create)/Authenticate User
+ /api/users/{username}/movies | GET | Gets a list of favourite movieId
+ /api/users/{username}/mustWatch | GET | Gets a list of mustWatch movieId

## Automated Testing.

[In this section, include a listing of the response from running your tests locally (npm run test). Simply copy the output from your terminal and paste it into a fenced block (the triple tilda markup, i.e. ~~~ ), as shown below - do not use a screenshot.]





## Deployments.

Specify the URLs of your deployments, both staging and production


![image-20240102113040159](C:\Users\江睿达\AppData\Roaming\Typora\typora-user-images\image-20240102113040159.png)

## Independent Learning (if relevant)

Sspecify the URL of the Coveralls webpage that contains your tests' code coverage metrics.

--------------------------------|---------|----------|---------|---------|-------------------
| File                           | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s |
| ------------------------------ | ------- | -------- | ------- | ------- | ----------------- |
| All files                      | 91.44   | 63.4     | 94.87   | 95.75   |                   |
| movies-api-cicd                | 100     | 100      | 100     | 100     |                   |
| index.js                       | 100     | 100      | 100     | 100     |                   |
| movies-api-cicd/api            | 87.42   | 60.76    | 93.47   | 94.59   |                   |
| tmdb-api.js                    | 87.42   | 60.76    | 93.47   | 94.59   | 21-29             |
| movies-api-cicd/api/movies     | 94.11   | 71.42    | 93.18   | 95.38   |                   |
| index.js                       | 93.75   | 70.58    | 92.85   | 94.91   | 70-72             |
| movieModel.js                  | 100     | 100      | 100     | 100     |                   |
| movies-api-cicd/api/people     | 100     | 66.66    | 100     | 100     |                   |
| index.js                       | 100     | 65.21    | 100     | 100     | 2,14              |
| peopleModel.js                 | 100     | 100      | 100     | 100     |                   |
| movies-api-cicd/api/users      | 87.91   | 63.9     | 94.73   | 93      |                   |
| index.js                       | 86.84   | 67.7     | 93.47   | 91.35   | 54,60-68          |
| userModel.js                   | 93.33   | 54.05    | 100     | 100     | 2-24              |
| movies-api-cicd/authenticate   | 90.62   | 62.16    | 100     | 100     |                   |
| index.js                       | 90.62   | 62.16    | 100     | 100     | 2-4,11-18         |
| movies-api-cicd/db             | 91.66   | 100      | 75      | 90.9    |                   |
| movies-api-cicd/errHandler     | 80      | 50       | 100     | 80      |                   |
| index.js                       | 80      | 50       | 100     | 80      | 5                 |
| movies-api-cicd/initialise-dev | 100     | 100      | 100     | 100     |                   |
| movies.js                      | 100     | 100      | 100     | 100     |                   |
| people.js                      | 100     | 100      | 100     | 100     |                   |
| users.js                       | 100     | 100      | 100     | 100     |                   |
| movies-api-cicd/seedData       | 94.91   | 52.17    | 100     | 100     |                   |
| index.js                       | 94.54   | 52.17    | 100     | 100     | 2-50              |
| movies.js                      | 100     | 100      | 100     | 100     |                   |
| people.js                      | 100     | 100      | 100     | 100     |                   |
--------------------------------|---------|----------|---------|---------|-------------------

State any other independent learning you achieved while completing this assignment.

During the process of learning how to generate code coverage reports for tests using the Istanbul npm package and publishing the reports to the Coveralls web service, I encountered some difficulties. I overcame these challenges by studying the official GitHub tutorial and other relevant materials. Ultimately, I revised my test code based on the coverage report, achieving a coverage rate of 96%.

![image-20240102154335285](C:\Users\江睿达\AppData\Roaming\Typora\typora-user-images\image-20240102154335285.png)