const people = [
    {
      "adult": false,
      "gender": 1,
      "id": 1548301,
      "known_for_department": "Acting",
      "name": "Farrah Mackenzie",
      "original_name": "Farrah Mackenzie",
      "popularity": 210.608,
      "profile_path": "/A0IxtjlDAiuXGR3kVSf1HunVFnC.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/tLsc8SCFO0rMVgVyNm9XtfnyX84.jpg",
          "id": 726209,
          "title": "Leave the World Behind",
          "original_language": "en",
          "original_title": "Leave the World Behind",
          "overview": "A family's getaway to a luxurious rental home takes an ominous turn when a cyberattack knocks out their devices—and two strangers appear at their door.",
          "poster_path": "/5o0zzKBRHE9to6g9tYweP54WJa2.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            9648,
            53,
            878
          ],
          "popularity": 426.365,
          "release_date": "2023-11-22",
          "video": false,
          "vote_average": 6.5,
          "vote_count": 1295
        },
        {
          "adult": false,
          "backdrop_path": "/d3Y9L11tgnYIKhAaRWfbaKGCjcM.jpg",
          "id": 399170,
          "title": "Logan Lucky",
          "original_language": "en",
          "original_title": "Logan Lucky",
          "overview": "Trying to reverse a family curse, brothers Jimmy and Clyde Logan set out to execute an elaborate robbery during the legendary Coca-Cola 600 race at the Charlotte Motor Speedway.",
          "poster_path": "/mQrhrBaaHvRfBQq0Px3HtVbH9iE.jpg",
          "media_type": "movie",
          "genre_ids": [
            35,
            80,
            28,
            18
          ],
          "popularity": 29.71,
          "release_date": "2017-08-17",
          "video": false,
          "vote_average": 6.73,
          "vote_count": 3271
        },
        {
          "adult": false,
          "backdrop_path": "/alsTIqT2ApBUUs8zEZEpmw1kDEa.jpg",
          "id": 412105,
          "title": "You Get Me",
          "original_language": "en",
          "original_title": "You Get Me",
          "overview": "After arguing with his girlfriend, Ali, Tyler lands in the arms of sexy new girl, Holly. The next morning, he finds that not only does Ali agree to take him back, but Holly is a new student at their school and is dead set on her new man.",
          "poster_path": "/5eV0mIQqSztD3McjO22EOFlQDj1.jpg",
          "media_type": "movie",
          "genre_ids": [
            53,
            10749,
            18
          ],
          "popularity": 21.672,
          "release_date": "2017-06-19",
          "video": false,
          "vote_average": 5.628,
          "vote_count": 1646
        }
      ]
    },
    {
      "adult": false,
      "gender": 1,
      "id": 115440,
      "known_for_department": "Acting",
      "name": "Sydney Sweeney",
      "original_name": "Sydney Sweeney",
      "popularity": 193.539,
      "profile_path": "/wrPmsC9YATcnyAxvXEdGshccbqU.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/9KnIzPCv9XpWA0MqmwiKBZvV1Sj.jpg",
          "id": 85552,
          "name": "Euphoria",
          "original_language": "en",
          "original_name": "Euphoria",
          "overview": "A group of high school students navigate love and friendships in a world of drugs, sex, trauma, and social media.",
          "poster_path": "/3Q0hd3heuWwDWpwcDkhQOA6TYWI.jpg",
          "media_type": "tv",
          "genre_ids": [
            18
          ],
          "popularity": 329.85,
          "first_air_date": "2019-06-16",
          "vote_average": 8.345,
          "vote_count": 9163,
          "origin_country": [
            "US"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/3rwszCm4XyBtIWSjbUbgNwJOidu.jpg",
          "id": 645710,
          "title": "The Voyeurs",
          "original_language": "en",
          "original_title": "The Voyeurs",
          "overview": "When Pippa and Thomas move into their dream apartment, they notice that their windows look directly into the apartment opposite – inviting them to witness the volatile relationship of the attractive couple across the street. But what starts as a simple curiosity turns into full-blown obsession with increasingly dangerous consequences.",
          "poster_path": "/8Y4XOIWhpOvSOEn8XrxbkH9yAXO.jpg",
          "media_type": "movie",
          "genre_ids": [
            53
          ],
          "popularity": 32.893,
          "release_date": "2021-08-25",
          "video": false,
          "vote_average": 6.662,
          "vote_count": 715
        },
        {
          "adult": false,
          "backdrop_path": "/oRiUKwDpcqDdoLwPoA4FIRh3hqY.jpg",
          "id": 466272,
          "title": "Once Upon a Time… in Hollywood",
          "original_language": "en",
          "original_title": "Once Upon a Time… in Hollywood",
          "overview": "Los Angeles, 1969. TV star Rick Dalton, a struggling actor specializing in westerns, and stuntman Cliff Booth, his best friend, try to survive in a constantly changing movie industry. Dalton is the neighbor of the young and promising actress and model Sharon Tate, who has just married the prestigious Polish director Roman Polanski…",
          "poster_path": "/8j58iEBw9pOXFD2L0nt0ZXeHviB.jpg",
          "media_type": "movie",
          "genre_ids": [
            35,
            18,
            53
          ],
          "popularity": 68.054,
          "release_date": "2019-07-24",
          "video": false,
          "vote_average": 7.44,
          "vote_count": 12478
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 192,
      "known_for_department": "Acting",
      "name": "Morgan Freeman",
      "original_name": "Morgan Freeman",
      "popularity": 168.712,
      "profile_path": "/jPsLqiYGSofU4s6BjrxnefMfabb.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/dYjZ27hDw2QFaEIfzbNGwW0IkV9.jpg",
          "id": 807,
          "title": "Se7en",
          "original_language": "en",
          "original_title": "Se7en",
          "overview": "Two homicide detectives are on a desperate hunt for a serial killer whose crimes are based on the \"seven deadly sins\" in this dark and haunting film that takes viewers from the tortured remains of one victim to the next. The seasoned Det. Sommerset researches each sin in an effort to get inside the killer's mind, while his novice partner, Mills, scoffs at his efforts to unravel the case.",
          "poster_path": "/6yoghtyTpznpBik8EngEmJskVUO.jpg",
          "media_type": "movie",
          "genre_ids": [
            80,
            9648,
            53
          ],
          "popularity": 91.37,
          "release_date": "1995-09-22",
          "video": false,
          "vote_average": 8.372,
          "vote_count": 19777
        },
        {
          "adult": false,
          "backdrop_path": "/kXfqcdQKsToO0OUXHcrrNCHDBzO.jpg",
          "id": 278,
          "title": "The Shawshank Redemption",
          "original_language": "en",
          "original_title": "The Shawshank Redemption",
          "overview": "Framed in the 1940s for the double murder of his wife and her lover, upstanding banker Andy Dufresne begins a new life at the Shawshank prison, where he puts his accounting skills to work for an amoral warden. During his long stretch in prison, Dufresne comes to be admired by the other inmates -- including an older prisoner named Red -- for his integrity and unquenchable sense of hope.",
          "poster_path": "/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            80
          ],
          "popularity": 147.836,
          "release_date": "1994-09-23",
          "video": false,
          "vote_average": 8.708,
          "vote_count": 25206
        },
        {
          "adult": false,
          "backdrop_path": "/ozVwXlfxqNsariipatGwa5px3Pm.jpg",
          "id": 240832,
          "title": "Lucy",
          "original_language": "en",
          "original_title": "Lucy",
          "overview": "A woman, accidentally caught in a dark deal, turns the tables on her captors and transforms into a merciless warrior evolved beyond human logic.",
          "poster_path": "/dhjyfcwEoW6jJ4Q7DpZTp6E58GA.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            878
          ],
          "popularity": 46.862,
          "release_date": "2014-07-25",
          "video": false,
          "vote_average": 6.444,
          "vote_count": 15348
        }
      ]
    },
    {
      "adult": false,
      "gender": 1,
      "id": 2359226,
      "known_for_department": "Acting",
      "name": "Aya Asahina",
      "original_name": "Aya Asahina",
      "popularity": 165.24,
      "profile_path": "/zF3la0KvayUV3uACYPiBgCRIQcI.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/bKxiLRPVWe2nZXCzt6JPr5HNWYm.jpg",
          "id": 110316,
          "name": "Alice in Borderland",
          "original_language": "ja",
          "original_name": "今際の国のアリス",
          "overview": "With his two friends, a video-game-obsessed young man finds himself in a strange version of Tokyo where they must compete in dangerous games to win.",
          "poster_path": "/20mOwAAPwZ1vLQkw0fvuQHiG7bO.jpg",
          "media_type": "tv",
          "genre_ids": [
            18,
            9648,
            10759
          ],
          "popularity": 404.881,
          "first_air_date": "2020-12-10",
          "vote_average": 8.185,
          "vote_count": 1738,
          "origin_country": [
            "JP"
          ]
        },
        {
          "adult": false,
          "backdrop_path": null,
          "id": 677602,
          "title": "Grand Blue",
          "original_language": "ja",
          "original_title": "ぐらんぶる",
          "overview": "Iori’s only dream is to go to college on a remote island—but when he gets roped into the school’s debaucherous, alcohol-indulgent diving club his hope for a sparkling campus life is thrown into chaos.",
          "poster_path": "/rSccMJVBg7Fn50tswLAaNSUuoUJ.jpg",
          "media_type": "movie",
          "genre_ids": [
            35
          ],
          "popularity": 10.64,
          "release_date": "2020-08-07",
          "video": false,
          "vote_average": 6,
          "vote_count": 12
        },
        {
          "adult": false,
          "backdrop_path": "/iNpbnWI2vQWGdPC9Pbt1RnV71R5.jpg",
          "id": 91414,
          "name": "Runway 24",
          "original_language": "ja",
          "original_name": "ランウェイ 24",
          "overview": "Inoue Momoko admires her late father, who was a pilot. She begins work as a co-pilot at a low-cost airline. Under Captain Shinkai Kohei’s instructions, who knew her father, Inoue Momoko works hard to become a captain and her boyfriend Umino Daisuke supports her dream too.\n\nOne day, Inoue Momoko has a problem with a passenger complaining about the limit for carry-on baggage. At that time, Katsuki Tetsuya looks at the situation.",
          "poster_path": "/2j2p3MF2GpSMHAFaYTgIRQeVOHa.jpg",
          "media_type": "tv",
          "genre_ids": [
            10751,
            18
          ],
          "popularity": 15.289,
          "first_air_date": "2019-07-06",
          "vote_average": 6.7,
          "vote_count": 3,
          "origin_country": [
            "JP"
          ]
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 64295,
      "known_for_department": "Acting",
      "name": "Alan Ritchson",
      "original_name": "Alan Ritchson",
      "popularity": 163.974,
      "profile_path": "/wdmLUSPEC7dXuqnjTM4NgbjvTKk.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/wrhLyiY7ksW0fQCqNpa52qiOAH8.jpg",
          "id": 108978,
          "name": "Reacher",
          "original_language": "en",
          "original_name": "Reacher",
          "overview": "Jack Reacher, a veteran military police investigator, has just recently entered civilian life. Reacher is a drifter, carrying no phone and the barest of essentials as he travels the country and explores the nation he once served.",
          "poster_path": "/jFuH0md41x5mB4qj5344mSmtHrO.jpg",
          "media_type": "tv",
          "genre_ids": [
            10759,
            80,
            18
          ],
          "popularity": 1369.848,
          "first_air_date": "2022-02-03",
          "vote_average": 8.085,
          "vote_count": 1033,
          "origin_country": [
            "US"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/eezsbzYPbYKjjh6E1XHDBNlLynh.jpg",
          "id": 98566,
          "title": "Teenage Mutant Ninja Turtles",
          "original_language": "en",
          "original_title": "Teenage Mutant Ninja Turtles",
          "overview": "When a kingpin threatens New York City, a group of mutated turtle warriors must emerge from the shadows to protect their home.",
          "poster_path": "/azL2ThbJMIkts3ZMt3j1YgBUeDB.jpg",
          "media_type": "movie",
          "genre_ids": [
            878,
            28,
            12,
            35
          ],
          "popularity": 81.173,
          "release_date": "2014-08-07",
          "video": false,
          "vote_average": 5.903,
          "vote_count": 6470
        },
        {
          "adult": false,
          "backdrop_path": "/oDYQhS5LtFXUP0x6bCZije5HRr9.jpg",
          "id": 101299,
          "title": "The Hunger Games: Catching Fire",
          "original_language": "en",
          "original_title": "The Hunger Games: Catching Fire",
          "overview": "Katniss Everdeen has returned home safe after winning the 74th Annual Hunger Games along with fellow tribute Peeta Mellark. Winning means that they must turn around and leave their family and close friends, embarking on a \"Victor's Tour\" of the districts. Along the way Katniss senses that a rebellion is simmering, but the Capitol is still very much in control as President Snow prepares the 75th Annual Hunger Games (The Quarter Quell) - a competition that could change Panem forever.",
          "poster_path": "/6Szz3g7ngNsYU2oPqsZwMzqUnLC.jpg",
          "media_type": "movie",
          "genre_ids": [
            12,
            28,
            878
          ],
          "popularity": 117.542,
          "release_date": "2013-11-15",
          "video": false,
          "vote_average": 7.4,
          "vote_count": 16518
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 1290466,
      "known_for_department": "Acting",
      "name": "Barry Keoghan",
      "original_name": "Barry Keoghan",
      "popularity": 155.559,
      "profile_path": "/ngoitknM6hw8fffLywyvjzy6Iti.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/ddIkmH3TpR6XSc47jj0BrGK5Rbz.jpg",
          "id": 374720,
          "title": "Dunkirk",
          "original_language": "en",
          "original_title": "Dunkirk",
          "overview": "The story of the miraculous evacuation of Allied soldiers from Belgium, Britain, Canada and France, who were cut off and surrounded by the German army from the beaches and harbour of Dunkirk between May 26th and June 4th 1940 during World War II.",
          "poster_path": "/b4Oe15CGLL61Ped0RAS9JpqdmCt.jpg",
          "media_type": "movie",
          "genre_ids": [
            10752,
            28,
            18
          ],
          "popularity": 36.327,
          "release_date": "2017-07-19",
          "video": false,
          "vote_average": 7.455,
          "vote_count": 15672
        },
        {
          "adult": false,
          "backdrop_path": "/c6H7Z4u73ir3cIoCteuhJh7UCAR.jpg",
          "id": 524434,
          "title": "Eternals",
          "original_language": "en",
          "original_title": "Eternals",
          "overview": "The Eternals are a team of ancient aliens who have been living on Earth in secret for thousands of years. When an unexpected tragedy forces them out of the shadows, they are forced to reunite against mankind’s most ancient enemy, the Deviants.",
          "poster_path": "/lFByFSLV5WDJEv3KabbdAF959F2.jpg",
          "media_type": "movie",
          "genre_ids": [
            878,
            28,
            12
          ],
          "popularity": 75.326,
          "release_date": "2021-11-03",
          "video": false,
          "vote_average": 6.9,
          "vote_count": 7679
        },
        {
          "adult": false,
          "backdrop_path": "/kJwQXgAk440l2vZOspUJee6inim.jpg",
          "id": 399057,
          "title": "The Killing of a Sacred Deer",
          "original_language": "en",
          "original_title": "The Killing of a Sacred Deer",
          "overview": "Dr. Steven Murphy is a renowned cardiovascular surgeon who presides over a spotless household with his wife and two children. Lurking at the margins of his idyllic suburban existence is Martin, a fatherless teen who insinuates himself into the doctor's life in gradually unsettling ways.",
          "poster_path": "/e4DGlsc9g0h5AyoyvvAuIRnofN7.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            53,
            9648
          ],
          "popularity": 38.33,
          "release_date": "2017-10-20",
          "video": false,
          "vote_average": 7.017,
          "vote_count": 3504
        }
      ]
    },
    {
      "adult": false,
      "gender": 1,
      "id": 568657,
      "known_for_department": "Acting",
      "name": "Sofia Boutella",
      "original_name": "Sofia Boutella",
      "popularity": 153.329,
      "profile_path": "/lGFhhjcjARQCM8AiGidyyyfDowh.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/yMlv1bD2fIkTnaJBkrkfS4IYHzM.jpg",
          "id": 207703,
          "title": "Kingsman: The Secret Service",
          "original_language": "en",
          "original_title": "Kingsman: The Secret Service",
          "overview": "The story of a super-secret spy organization that recruits an unrefined but promising street kid into the agency's ultra-competitive training program just as a global threat emerges from a twisted tech genius.",
          "poster_path": "/ay7xwXn1G9fzX9TUBlkGA584rGi.jpg",
          "media_type": "movie",
          "genre_ids": [
            80,
            35,
            28,
            12
          ],
          "popularity": 71.876,
          "release_date": "2014-12-13",
          "video": false,
          "vote_average": 7.637,
          "vote_count": 15859
        },
        {
          "adult": false,
          "backdrop_path": "/i4ZougHEyBAboDpi6jsaTUDTjUl.jpg",
          "id": 282035,
          "title": "The Mummy",
          "original_language": "en",
          "original_title": "The Mummy",
          "overview": "Though safely entombed in a crypt deep beneath the unforgiving desert, an ancient queen whose destiny was unjustly taken from her is awakened in our current day, bringing with her malevolence grown over millennia, and terrors that defy human comprehension.",
          "poster_path": "/zxkY8byBnCsXodEYpK8tmwEGXBI.jpg",
          "media_type": "movie",
          "genre_ids": [
            14,
            53,
            28,
            12,
            27
          ],
          "popularity": 36.017,
          "release_date": "2017-06-06",
          "video": false,
          "vote_average": 5.509,
          "vote_count": 6833
        },
        {
          "adult": false,
          "backdrop_path": "/v0sblc9A8eaE8EqDQ5Y6fELj4oB.jpg",
          "id": 507076,
          "title": "Climax",
          "original_language": "fr",
          "original_title": "Climax",
          "overview": "When a dance troupe is lured to an empty school, a bowl of drug-laced sangria causes their jubilant rehearsal to descend into a dark and explosive nightmare as they try to survive the night—and find who's responsible—before it's too late.",
          "poster_path": "/47IXH2iEWwX0F7vIyGXaKQ0psBG.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            27
          ],
          "popularity": 25.467,
          "release_date": "2018-09-19",
          "video": false,
          "vote_average": 7.097,
          "vote_count": 1913
        }
      ]
    },
    {
      "adult": false,
      "gender": 1,
      "id": 974169,
      "known_for_department": "Acting",
      "name": "Jenna Ortega",
      "original_name": "Jenna Ortega",
      "popularity": 142.351,
      "profile_path": "/7oUAtVgZU0uLdUSvDHKkINt1y7Y.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/iHSwvRVsRyxpX7FE7GbviaDvgGZ.jpg",
          "id": 119051,
          "name": "Wednesday",
          "original_language": "en",
          "original_name": "Wednesday",
          "overview": "Wednesday Addams is sent to Nevermore Academy, a bizarre boarding school where she attempts to master her psychic powers, stop a monstrous killing spree of the town citizens, and solve the supernatural mystery that affected her family 25 years ago — all while navigating her new relationships.",
          "poster_path": "/9PFonBhy4cQy7Jz20NpMygczOkv.jpg",
          "media_type": "tv",
          "genre_ids": [
            10765,
            9648,
            35
          ],
          "popularity": 463.638,
          "first_air_date": "2022-11-23",
          "vote_average": 8.52,
          "vote_count": 7816,
          "origin_country": [
            "US"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/ifUfE79O1raUwbaQRIB7XnFz5ZC.jpg",
          "id": 646385,
          "title": "Scream",
          "original_language": "en",
          "original_title": "Scream",
          "overview": "Twenty-five years after a streak of brutal murders shocked the quiet town of Woodsboro, a new killer has donned the Ghostface mask and begins targeting a group of teenagers to resurrect secrets from the town’s deadly past.",
          "poster_path": "/971Kqs1q4nuSc9arn1QAuKYbfxy.jpg",
          "media_type": "movie",
          "genre_ids": [
            27,
            9648,
            53
          ],
          "popularity": 69.18,
          "release_date": "2022-01-12",
          "video": false,
          "vote_average": 6.692,
          "vote_count": 2920
        },
        {
          "adult": false,
          "backdrop_path": "/70Rm9ItxKuEKN8iu6rNjfwAYUCJ.jpg",
          "id": 760104,
          "title": "X",
          "original_language": "en",
          "original_title": "X",
          "overview": "In 1979, a group of young filmmakers set out to make an adult film in rural Texas, but when their reclusive, elderly hosts catch them in the act, the cast find themselves fighting for their lives.",
          "poster_path": "/woTQx9Q4b8aO13jR9dsj8C9JESy.jpg",
          "media_type": "movie",
          "genre_ids": [
            27,
            53,
            9648
          ],
          "popularity": 127.962,
          "release_date": "2022-03-17",
          "video": false,
          "vote_average": 6.75,
          "vote_count": 2488
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 1818455,
      "known_for_department": "Acting",
      "name": "Van Crosby",
      "original_name": "Van Crosby",
      "popularity": 130.069,
      "profile_path": "/kzMdxbfq5rNU4du4AbB7NPQ4ICG.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/r9bIkU9nXZUWSlDUgDMUcDrlK0A.jpg",
          "id": 1029575,
          "title": "The Family Plan",
          "original_language": "en",
          "original_title": "The Family Plan",
          "overview": "Dan Morgan is many things: a devoted husband, a loving father, a celebrated car salesman. He's also a former assassin. And when his past catches up to his present, he's forced to take his unsuspecting family on a road trip unlike any other.",
          "poster_path": "/jLLtx3nTRSLGPAKl4RoIv1FbEBr.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            35
          ],
          "popularity": 954.371,
          "release_date": "2023-12-14",
          "video": false,
          "vote_average": 7.366,
          "vote_count": 464
        },
        {
          "adult": false,
          "backdrop_path": "/5OXWO7PGnNUUOEcBM8R8b4bZKZS.jpg",
          "id": 71764,
          "name": "Splitting Up Together",
          "original_language": "en",
          "original_name": "Splitting Up Together",
          "overview": "Lena and Martin were once madly in love. But, like many marriages, time and circumstance eventually took their toll, and they decide that everyone's lives would be better if they got a divorce. Facing a daunting real estate market, the couple decide not to sell their house and to \"Bird Nest\" instead. The 'on-duty' parent will live in the house while the 'off-duty' parent will live in the detached garage. As Lena begins to dip her toes into the dating waters, Martin begins to see his own culpability in his marriage falling apart.",
          "poster_path": "/jMxlolm8TcNKNNMRyjxWqvpiIQ8.jpg",
          "media_type": "tv",
          "genre_ids": [
            10751,
            35
          ],
          "popularity": 420.172,
          "first_air_date": "2018-03-27",
          "vote_average": 6.5,
          "vote_count": 22,
          "origin_country": [
            "US"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/65Y6PweSvQ1OOFBzStybjipURRP.jpg",
          "id": 4057,
          "name": "Criminal Minds",
          "original_language": "en",
          "original_name": "Criminal Minds",
          "overview": "An elite team of FBI profilers analyze the country's most twisted criminal minds, anticipating their next moves before they strike again. The Behavioral Analysis Unit's most experienced agent is David Rossi, a founding member of the BAU who returns to help the team solve new cases.",
          "poster_path": "/7TCwgX7oQKxcWYEhSPRmaHe6ULN.jpg",
          "media_type": "tv",
          "genre_ids": [
            80,
            18,
            9648
          ],
          "popularity": 3220.474,
          "first_air_date": "2005-09-22",
          "vote_average": 8.32,
          "vote_count": 3494,
          "origin_country": [
            "US"
          ]
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 12799,
      "known_for_department": "Acting",
      "name": "Jeremy Piven",
      "original_name": "Jeremy Piven",
      "popularity": 128.986,
      "profile_path": "/pqdR8zqAWF87chGYlbdYr0YfC7g.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/sd4xN5xi8tKRPrJOWwNiZEile7f.jpg",
          "id": 920,
          "title": "Cars",
          "original_language": "en",
          "original_title": "Cars",
          "overview": "Lightning McQueen, a hotshot rookie race car driven to succeed, discovers that life is about the journey, not the finish line, when he finds himself unexpectedly detoured in the sleepy Route 66 town of Radiator Springs. On route across the country to the big Piston Cup Championship in California to compete against two seasoned pros, McQueen gets to know the town's offbeat characters.",
          "poster_path": "/abW5AzHDaIK1n9C36VdAeOwORRA.jpg",
          "media_type": "movie",
          "genre_ids": [
            16,
            12,
            35,
            10751
          ],
          "popularity": 62.788,
          "release_date": "2006-06-08",
          "video": false,
          "vote_average": 6.9,
          "vote_count": 13106
        },
        {
          "adult": false,
          "backdrop_path": "/gU7EMTMV3d2ykb9630PAwLSvtSz.jpg",
          "id": 9778,
          "title": "Serendipity",
          "original_language": "en",
          "original_title": "Serendipity",
          "overview": "Although strangers Sara and Jonathan are both already in relationships, they realize they have genuine chemistry after a chance encounter – but part company soon after. Years later, they each yearn to reunite, despite being destined for the altar. But to give true love a chance, they have to find one another again.",
          "poster_path": "/srCE5lEIjVEG5eqWg9JcjZCK1aQ.jpg",
          "media_type": "movie",
          "genre_ids": [
            35,
            10749,
            18
          ],
          "popularity": 29.568,
          "release_date": "2001-10-05",
          "video": false,
          "vote_average": 6.913,
          "vote_count": 1784
        },
        {
          "adult": false,
          "backdrop_path": "/6bNQuKFJIamipvYclG4MEbDSLM5.jpg",
          "id": 93837,
          "title": "So Undercover",
          "original_language": "en",
          "original_title": "So Undercover",
          "overview": "When the FBI hires her to go undercover at a college sorority, Molly Morris (Miley Cyrus) must transform herself from a tough, streetwise private investigator to a refined, sophisticated university girl to help protect the daughter of a one-time Mobster. With several suspects on her list, Molly unexpectedly discovers that not everyone is who they appear to be, including herself.",
          "poster_path": "/abR6e0FaWwwcEJEM4PY5VvjwBr1.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            35
          ],
          "popularity": 19.567,
          "release_date": "2012-12-06",
          "video": false,
          "vote_average": 6.2,
          "vote_count": 1084
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 18897,
      "known_for_department": "Acting",
      "name": "Jackie Chan",
      "original_name": "Jackie Chan",
      "popularity": 125.283,
      "profile_path": "/nraZoTzwJQPHspAVsKfgl3RXKKa.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/r4yFYBEcV247B9VXi1307fIhVqN.jpg",
          "id": 2109,
          "title": "Rush Hour",
          "original_language": "en",
          "original_title": "Rush Hour",
          "overview": "When Hong Kong Inspector Lee is summoned to Los Angeles to investigate a kidnapping, the FBI doesn't want any outside help and assigns cocky LAPD Detective James Carter to distract Lee from the case. Not content to watch the action from the sidelines, Lee and Carter form an unlikely partnership and investigate the case themselves.",
          "poster_path": "/we7wOLVFgxhzLzUt0qNe50xdIQZ.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            35,
            80
          ],
          "popularity": 49.586,
          "release_date": "1998-09-18",
          "video": false,
          "vote_average": 7.023,
          "vote_count": 4488
        },
        {
          "adult": false,
          "backdrop_path": "/zzFTSEAZcLGSbGipQVflSNUqpij.jpg",
          "id": 5175,
          "title": "Rush Hour 2",
          "original_language": "en",
          "original_title": "Rush Hour 2",
          "overview": "It's vacation time for Carter as he finds himself alongside Lee in Hong Kong wishing for more excitement. While Carter wants to party and meet the ladies, Lee is out to track down a Triad gang lord who may be responsible for killing two men at the American Embassy. Things get complicated as the pair stumble onto a counterfeiting plot. The boys are soon up to their necks in fist fights and life-threatening situations. A trip back to the U.S. may provide the answers about the bombing, the counterfeiting, and the true allegiance of sexy customs agent Isabella.",
          "poster_path": "/aBQf2vMiCINeVC9v6BGVYKXurTh.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            35,
            80
          ],
          "popularity": 47.883,
          "release_date": "2001-08-03",
          "video": false,
          "vote_average": 6.719,
          "vote_count": 3761
        },
        {
          "adult": false,
          "backdrop_path": "/ozsLB1HRCN6ZAmJN89pWtoiAwnb.jpg",
          "id": 5174,
          "title": "Rush Hour 3",
          "original_language": "en",
          "original_title": "Rush Hour 3",
          "overview": "After a botched assassination attempt, the mismatched duo finds themselves in Paris, struggling to retrieve a precious list of names, as the murderous crime syndicate's henchmen try their best to stop them. Once more, Lee and Carter must fight their way through dangerous gangsters; however, this time, the past has come back to haunt Lee. Will the boys get the job done once and for all?",
          "poster_path": "/iXsbAIZezI0gh0dnOnBbeFuG3AO.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            35,
            80
          ],
          "popularity": 55.982,
          "release_date": "2007-08-08",
          "video": false,
          "vote_average": 6.441,
          "vote_count": 2978
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 1190668,
      "known_for_department": "Acting",
      "name": "Timothée Chalamet",
      "original_name": "Timothée Chalamet",
      "popularity": 124.65,
      "profile_path": "/BE2sdjpgsa2rNTFa66f7upkaOP.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/zvOJawrnmgK0sL293mOXOdLvTXQ.jpg",
          "id": 398818,
          "title": "Call Me by Your Name",
          "original_language": "en",
          "original_title": "Call Me by Your Name",
          "overview": "In 1980s Italy, a relationship begins between seventeen-year-old teenage Elio and the older adult man hired as his father's research assistant.",
          "poster_path": "/mZ4gBdfkhP9tvLH1DO4m4HYtiyi.jpg",
          "media_type": "movie",
          "genre_ids": [
            10749,
            18
          ],
          "popularity": 76.607,
          "release_date": "2017-09-01",
          "video": false,
          "vote_average": 8.2,
          "vote_count": 11487
        },
        {
          "adult": false,
          "backdrop_path": "/jYEW5xZkZk2WTrdbMGAPFuBqbDc.jpg",
          "id": 438631,
          "title": "Dune",
          "original_language": "en",
          "original_title": "Dune",
          "overview": "Paul Atreides, a brilliant and gifted young man born into a great destiny beyond his understanding, must travel to the most dangerous planet in the universe to ensure the future of his family and his people. As malevolent forces explode into conflict over the planet's exclusive supply of the most precious resource in existence-a commodity capable of unlocking humanity's greatest potential-only those who can conquer their fear will survive.",
          "poster_path": "/d5NXSklXo0qyIYkgV94XAgMIckC.jpg",
          "media_type": "movie",
          "genre_ids": [
            878,
            12
          ],
          "popularity": 111.735,
          "release_date": "2021-09-15",
          "video": false,
          "vote_average": 7.784,
          "vote_count": 9759
        },
        {
          "adult": false,
          "backdrop_path": "/urwWVsciRiNSmhtMJcck0noWAXr.jpg",
          "id": 504949,
          "title": "The King",
          "original_language": "en",
          "original_title": "The King",
          "overview": "England, 15th century. Hal, a capricious prince who lives among the populace far from court, is forced by circumstances to reluctantly accept the throne and become Henry V.",
          "poster_path": "/8u0QBGUbZcBW59VEAdmeFl9g98N.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            36,
            10752
          ],
          "popularity": 24.503,
          "release_date": "2019-10-11",
          "video": false,
          "vote_average": 7.153,
          "vote_count": 2977
        }
      ]
    },
    {
      "adult": false,
      "gender": 1,
      "id": 2112442,
      "known_for_department": "Acting",
      "name": "Ellie James",
      "original_name": "Ellie James",
      "popularity": 122.722,
      "profile_path": "/sfWWp69KlcxuIuymP7jOuW49FS7.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/6jRHaYJyje5RNS3L353udrPU3ME.jpg",
          "id": 554230,
          "title": "The Lost Daughter",
          "original_language": "en",
          "original_title": "The Lost Daughter",
          "overview": "A woman's seaside vacation takes a dark turn when her obsession with a young mother forces her to confront secrets from her past.",
          "poster_path": "/t1oLNRFixpFOVsyz1HCqCUW3wiW.jpg",
          "media_type": "movie",
          "genre_ids": [
            18
          ],
          "popularity": 21.318,
          "release_date": "2021-12-16",
          "video": false,
          "vote_average": 6.568,
          "vote_count": 832
        },
        {
          "adult": false,
          "backdrop_path": "/4XcOdRYPTdNyu5C1iDnFHDqI9QN.jpg",
          "id": 64073,
          "name": "Class",
          "original_language": "en",
          "original_name": "Class",
          "overview": "Coal Hill School has been a feature of Doctor Who since the first episode, but now we get to see the day-to-day adventures of the students coping with intrusions from space and time.",
          "poster_path": "/74W4oKwZR71cXv76R6BsAPMlTr7.jpg",
          "media_type": "tv",
          "genre_ids": [
            18,
            10765
          ],
          "popularity": 34.121,
          "first_air_date": "2016-10-22",
          "vote_average": 6.401,
          "vote_count": 106,
          "origin_country": [
            "GB"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/1dj1BYNBt4bictaUJPbszPiDCnc.jpg",
          "id": 99737,
          "name": "The Winter King",
          "original_language": "en",
          "original_name": "The Winter King",
          "overview": "Bastard son Arthur returns to the land he's been banished from to find a kingdom in chaos. Seeking the aid of his oldest confidants, Arthur aims to right Britain's path as he faces the most unexpected of obstacles.",
          "poster_path": "/fYx5q9hPAmbDrWzsYUDhxZJnigC.jpg",
          "media_type": "tv",
          "genre_ids": [
            18
          ],
          "popularity": 101.592,
          "first_air_date": "2023-12-21",
          "vote_average": 7.2,
          "vote_count": 4,
          "origin_country": [
            "GB"
          ]
        }
      ]
    },
    {
      "adult": false,
      "gender": 0,
      "id": 3234630,
      "known_for_department": "Acting",
      "name": "Sangeeth Shobhan",
      "original_name": "Sangeeth Shobhan",
      "popularity": 119.583,
      "profile_path": "/7Vox31bH7XmgPNJzMKGa4uGyjW8.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/1jof3bGVg67HLmvRHfTzqb2IODO.jpg",
          "id": 138179,
          "name": "Oka Chinna Family Story",
          "original_language": "te",
          "original_name": "ఒక చిన్న Family Story",
          "overview": "Mahesh and his mother embark on an adventurous journey filled with hilarious situations as they try to make quick money to repay a huge loan.",
          "poster_path": "/u1Tq2Qqb1oUJ6WSzVJqWk03LzEl.jpg",
          "media_type": "tv",
          "genre_ids": [
            35,
            10751
          ],
          "popularity": 21.273,
          "first_air_date": "2021-11-19",
          "vote_average": 7.5,
          "vote_count": 2,
          "origin_country": [
            "IN"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/jBnnkkXRZ0pV3Tw31Z2ALO638wA.jpg",
          "id": 1187075,
          "title": "MAD",
          "original_language": "te",
          "original_title": "MAD",
          "overview": "Set in an engineering college and revolves around the antics of the students there, primarily the boys, who get a kick out of torturing the hostel warden.",
          "poster_path": "/nDpOmgBfQZwOpFBcgokQGqd74r1.jpg",
          "media_type": "movie",
          "genre_ids": [
            35,
            10749,
            18
          ],
          "popularity": 4.754,
          "release_date": "2023-10-06",
          "video": false,
          "vote_average": 7,
          "vote_count": 4
        },
        {
          "adult": false,
          "backdrop_path": "/d7jfcyPb5ZncLyhPFNjpuIeeZ1y.jpg",
          "id": 1119091,
          "title": "Prema Vimanam",
          "original_language": "te",
          "original_title": "ప్రేమ విమానం",
          "overview": "Two kids with a dream to board a flight cross paths with a young couple who must urgently catch the flight to start a new life.",
          "poster_path": "/9eljOANAd6HafUDdmp3xnmkpnt8.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            35
          ],
          "popularity": 1.874,
          "release_date": "2023-10-13",
          "video": false,
          "vote_average": 7,
          "vote_count": 1
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 976,
      "known_for_department": "Acting",
      "name": "Jason Statham",
      "original_name": "Jason Statham",
      "popularity": 118.976,
      "profile_path": "/whNwkEQYWLFJA8ij0WyOOAD5xhQ.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/ysKahAEPP8h6MInuLjr0xuZOTjh.jpg",
          "id": 107,
          "title": "Snatch",
          "original_language": "en",
          "original_title": "Snatch",
          "overview": "Unscrupulous boxing promoters, violent bookmakers, a Russian gangster, incompetent amateur robbers and supposedly Jewish jewelers fight to track down a priceless stolen diamond.",
          "poster_path": "/56mOJth6DJ6JhgoE2jtpilVqJO.jpg",
          "media_type": "movie",
          "genre_ids": [
            80,
            35
          ],
          "popularity": 37.095,
          "release_date": "2000-09-01",
          "video": false,
          "vote_average": 7.807,
          "vote_count": 8481
        },
        {
          "adult": false,
          "backdrop_path": "/iP1cjN3ZGs1yJXROKaUj9hi1yF2.jpg",
          "id": 345940,
          "title": "The Meg",
          "original_language": "en",
          "original_title": "The Meg",
          "overview": "A deep sea submersible pilot revisits his past fears in the Mariana Trench, and accidentally unleashes the seventy foot ancestor of the Great White Shark believed to be extinct.",
          "poster_path": "/eyWICPcxOuTcDDDbTMOZawoOn8d.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            878,
            27
          ],
          "popularity": 73.2,
          "release_date": "2018-08-09",
          "video": false,
          "vote_average": 6.255,
          "vote_count": 7196
        },
        {
          "adult": false,
          "backdrop_path": "/kWt1OcPgwO1ssu57wgTKmq38JYw.jpg",
          "id": 4108,
          "title": "The Transporter",
          "original_language": "en",
          "original_title": "The Transporter",
          "overview": "Former Special Forces officer, Frank Martin will deliver anything to anyone for the right price, and his no-questions-asked policy puts him in high demand. But when he realizes his latest cargo is alive, it sets in motion a dangerous chain of events. The bound and gagged Lai is being smuggled to France by a shady American businessman, and Frank works to save her as his own illegal activities are uncovered by a French detective.",
          "poster_path": "/v3QIFUWgtVN4wejVuDZowuyJ20W.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            80,
            53
          ],
          "popularity": 38.161,
          "release_date": "2002-10-02",
          "video": false,
          "vote_average": 6.697,
          "vote_count": 4991
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 1715408,
      "known_for_department": "Acting",
      "name": "Vikram Kochhar",
      "original_name": "Vikram Kochhar",
      "popularity": 107.263,
      "profile_path": "/qzwC8ebdicpuoVjfsM3KQSG6J9O.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/notcxmT6RN93g43BnRI8GvRktZ.jpg",
          "id": 498448,
          "title": "Kesari",
          "original_language": "hi",
          "original_title": "केसरी",
          "overview": "In 1897, an army of 21 Sikhs battles 10,000 Afghans to prevent the Saragarhi Fort from being taken down.",
          "poster_path": "/ctjT1pMNAGD9ou0kuhz806tf7kX.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            18,
            36,
            10752
          ],
          "popularity": 11.687,
          "release_date": "2019-03-21",
          "video": false,
          "vote_average": 7,
          "vote_count": 91
        },
        {
          "adult": false,
          "backdrop_path": "/5R19VzUX3unBGgWAMmCJKLcOsbo.jpg",
          "id": 960876,
          "title": "Dunki",
          "original_language": "hi",
          "original_title": "डंकी",
          "overview": "Four friends from a sleepy little village in Punjab share a common dream: to go to England. Their problem is that they have neither the visa nor the ticket. A soldier alights from a train one day, and their lives change. He gives them a soldier's promise: He will take them to the land of their dreams. What follows is a hilarious and heartwarming tale of a perilous journey through the desert and the sea, but most crucially through the hinterlands of their mind.",
          "poster_path": "/jj9ue91o7dVJNUL8pOMDjO0udfK.jpg",
          "media_type": "movie",
          "genre_ids": [
            35,
            18
          ],
          "popularity": 41.966,
          "release_date": "2023-12-21",
          "video": false,
          "vote_average": 6.6,
          "vote_count": 21
        },
        {
          "adult": false,
          "backdrop_path": "/vA5tZeY30S8GPsRz5CeS4VPNYWZ.jpg",
          "id": 783723,
          "title": "Thank God",
          "original_language": "hi",
          "original_title": "थैंक गॉड",
          "overview": "An egoistic real estate broker in huge debts, meets with an accident. As he gains consciousness, he realizes that he is in heaven. God appears before him and informs him that he must play a 'Game of Life'. If he manages to win, he will be sent back to earth; if he loses, he will be sent to hell.",
          "poster_path": "/dxKtwP2Q4n3jfqMIqxoShX08UkH.jpg",
          "media_type": "movie",
          "genre_ids": [
            35
          ],
          "popularity": 4.999,
          "release_date": "2022-10-25",
          "video": false,
          "vote_average": 6.761,
          "vote_count": 24
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 3765540,
      "known_for_department": "Acting",
      "name": "Stuart Campbell",
      "original_name": "Stuart Campbell",
      "popularity": 101.601,
      "profile_path": "/tHfoXNuaIr7T0CvM6l55NTr40Ay.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/jyvOp7oWNueMQsIUZCiHtMppQC2.jpg",
          "id": 93870,
          "name": "SAS: Rogue Heroes",
          "original_language": "en",
          "original_name": "SAS: Rogue Heroes",
          "overview": "The dramatised account of how the world’s greatest Special Forces unit, the SAS, was formed under extraordinary circumstances in the darkest days of World War Two.",
          "poster_path": "/qEaxiDrPaTY34eIg6naXMfM2IKC.jpg",
          "media_type": "tv",
          "genre_ids": [
            18,
            10768
          ],
          "popularity": 69.055,
          "first_air_date": "2022-10-30",
          "vote_average": 7.578,
          "vote_count": 64,
          "origin_country": [
            "GB"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/nf3Vlxm3C9U1aKUUQHmKFZmxPSc.jpg",
          "id": 56570,
          "name": "Outlander",
          "original_language": "en",
          "original_name": "Outlander",
          "overview": "The story of Claire Randall, a married combat nurse from 1945 who is mysteriously swept back in time to 1743, where she is immediately thrown into an unknown world where her life is threatened. When she is forced to marry Jamie, a chivalrous and romantic young Scottish warrior, a passionate affair is ignited that tears Claire's heart between two vastly different men in two irreconcilable lives.",
          "poster_path": "/oftZNfyTVNU7IfOqoGLoT8MGvNs.jpg",
          "media_type": "tv",
          "genre_ids": [
            18,
            10765
          ],
          "popularity": 838.877,
          "first_air_date": "2014-08-09",
          "vote_average": 8.2,
          "vote_count": 2443,
          "origin_country": [
            "US"
          ]
        },
        {
          "adult": false,
          "backdrop_path": "/1dj1BYNBt4bictaUJPbszPiDCnc.jpg",
          "id": 99737,
          "name": "The Winter King",
          "original_language": "en",
          "original_name": "The Winter King",
          "overview": "Bastard son Arthur returns to the land he's been banished from to find a kingdom in chaos. Seeking the aid of his oldest confidants, Arthur aims to right Britain's path as he faces the most unexpected of obstacles.",
          "poster_path": "/fYx5q9hPAmbDrWzsYUDhxZJnigC.jpg",
          "media_type": "tv",
          "genre_ids": [
            18
          ],
          "popularity": 101.592,
          "first_air_date": "2023-12-21",
          "vote_average": 7.2,
          "vote_count": 4,
          "origin_country": [
            "GB"
          ]
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 64,
      "known_for_department": "Acting",
      "name": "Gary Oldman",
      "original_name": "Gary Oldman",
      "popularity": 101.377,
      "profile_path": "/yhaSM5habNNI1Tf4ALRwRk3VvSZ.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/nMKdUUepR0i5zn0y1T4CsSB5chy.jpg",
          "id": 155,
          "title": "The Dark Knight",
          "original_language": "en",
          "original_title": "The Dark Knight",
          "overview": "Batman raises the stakes in his war on crime. With the help of Lt. Jim Gordon and District Attorney Harvey Dent, Batman sets out to dismantle the remaining criminal organizations that plague the streets. The partnership proves to be effective, but they soon find themselves prey to a reign of chaos unleashed by a rising criminal mastermind known to the terrified citizens of Gotham as the Joker.",
          "poster_path": "/qJ2tW6WMUDux911r6m7haRef0WH.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            28,
            80,
            53
          ],
          "popularity": 126.226,
          "release_date": "2008-07-16",
          "video": false,
          "vote_average": 8.513,
          "vote_count": 31148
        },
        {
          "adult": false,
          "backdrop_path": "/c3OHQncTAnKFhdOTX7D3LTW6son.jpg",
          "id": 49026,
          "title": "The Dark Knight Rises",
          "original_language": "en",
          "original_title": "The Dark Knight Rises",
          "overview": "Following the death of District Attorney Harvey Dent, Batman assumes responsibility for Dent's crimes to protect the late attorney's reputation and is subsequently hunted by the Gotham City Police Department. Eight years later, Batman encounters the mysterious Selina Kyle and the villainous Bane, a new terrorist leader who overwhelms Gotham's finest. The Dark Knight resurfaces to protect a city that has branded him an enemy.",
          "poster_path": "/hr0L2aueqlP2BYUblTTjmtn0hw4.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            80,
            18,
            53
          ],
          "popularity": 111.188,
          "release_date": "2012-07-17",
          "video": false,
          "vote_average": 7.777,
          "vote_count": 21646
        },
        {
          "adult": false,
          "backdrop_path": "/zXwFJMwvQcJFitP9GcHZvHAHGe8.jpg",
          "id": 399404,
          "title": "Darkest Hour",
          "original_language": "en",
          "original_title": "Darkest Hour",
          "overview": "In May 1940, the fate of World War II hangs on Winston Churchill, who must decide whether to negotiate with Adolf Hitler or fight on knowing that it could mean the end of the British Empire.",
          "poster_path": "/xa6G3aKlysQeVg9wOb0dRcIGlWu.jpg",
          "media_type": "movie",
          "genre_ids": [
            18,
            36
          ],
          "popularity": 23.825,
          "release_date": "2017-11-22",
          "video": false,
          "vote_average": 7.353,
          "vote_count": 4828
        }
      ]
    },
    {
      "adult": false,
      "gender": 1,
      "id": 3161297,
      "known_for_department": "Acting",
      "name": "Nilja K. Baby",
      "original_name": "Nilja K. Baby",
      "popularity": 101.359,
      "profile_path": "/otlxmUL1bthUNpbo0JZ1MZ6402c.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/9sdVzdpifqINn94MtOElR5P3UQ8.jpg",
          "id": 816586,
          "title": "Malayankunju",
          "original_language": "ml",
          "original_title": "മലയൻകുഞ്ഞ്",
          "overview": "Anikkuttan is an ill-tempered electronics technician who leads a quiet life with his mother. His daily routines are disrupted when his next door neighbour's new born baby stirs up undesirable memories from his past. What will happen next?",
          "poster_path": "/qgrpQbOpHCi8wSVY4A6vweSTD0Y.jpg",
          "media_type": "movie",
          "genre_ids": [
            53,
            18
          ],
          "popularity": 2.884,
          "release_date": "2022-07-22",
          "video": false,
          "vote_average": 5.9,
          "vote_count": 17
        },
        {
          "adult": false,
          "backdrop_path": "/nH41REzItDOt5WlB6fD7sOeslYl.jpg",
          "id": 713854,
          "title": "Kappela",
          "original_language": "ml",
          "original_title": "കപ്പേള",
          "overview": "Falling into an over-the-phone romance with a rickshaw driver, a young woman visits his city when an encounter with a stranger derails their meeting.",
          "poster_path": "/5e6yMw6ipz7DvLbtKAvHjbQAjHr.jpg",
          "media_type": "movie",
          "genre_ids": [
            10749,
            9648
          ],
          "popularity": 4.125,
          "release_date": "2020-03-06",
          "video": false,
          "vote_average": 6.5,
          "vote_count": 18
        },
        {
          "adult": false,
          "backdrop_path": "/jpuNdfWxzeL04YHvgZvImbNno0R.jpg",
          "id": 889136,
          "title": "Freedom Fight",
          "original_language": "ml",
          "original_title": "സ്വാതന്ത്ര്യ സമരം",
          "overview": "A girl juggling her dreams and social expectations, women workers denied bathroom breaks, a housewife thrown into crisis by a routine gone awry, a maid caught between the priorities of her employer couple, a sewage worker deciding to question an atrocity. Five stories about people of different classes, genders and ages - connected in their struggles for freedom and by the tears and smiles they encounter in that journey.",
          "poster_path": "/hbLy7ZRfk6OOJ6LPANj9y3N2cKY.jpg",
          "media_type": "movie",
          "genre_ids": [
            18
          ],
          "popularity": 2.292,
          "release_date": "2022-02-11",
          "video": false,
          "vote_average": 7.3,
          "vote_count": 5
        }
      ]
    },
    {
      "adult": false,
      "gender": 2,
      "id": 16483,
      "known_for_department": "Acting",
      "name": "Sylvester Stallone",
      "original_name": "Sylvester Stallone",
      "popularity": 100.285,
      "profile_path": "/mJHmLVLctqZd30CSEUKuVFEPrnt.jpg",
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/tMpeVt7T2S2NaeEaNCUunCm7laX.jpg",
          "id": 27578,
          "title": "The Expendables",
          "original_language": "en",
          "original_title": "The Expendables",
          "overview": "Barney Ross leads a band of highly skilled mercenaries including knife enthusiast Lee Christmas, a martial arts expert Yin Yang, heavy weapons specialist Hale Caesar, demolitionist Toll Road, and a loose-cannon sniper Gunner Jensen. When the group is commissioned by the mysterious Mr. Church to assassinate the dictator of a small South American island, Barney and Lee visit the remote locale to scout out their opposition and discover the true nature of the conflict engulfing the city.",
          "poster_path": "/j09ZkH6R4JWVylBcDai1laCmGw7.jpg",
          "media_type": "movie",
          "genre_ids": [
            53,
            12,
            28
          ],
          "popularity": 51.592,
          "release_date": "2010-08-03",
          "video": false,
          "vote_average": 6.238,
          "vote_count": 7357
        },
        {
          "adult": false,
          "backdrop_path": "/kK9v1wclQxug6ZUJucD4DTaHgVF.jpg",
          "id": 1366,
          "title": "Rocky",
          "original_language": "en",
          "original_title": "Rocky",
          "overview": "An uneducated collector for a Philadelphia loan shark is given a once-in-a-lifetime opportunity to fight against the world heavyweight boxing champion.",
          "poster_path": "/cqxg1CihGR5ge0i1wYXr4Rdeppu.jpg",
          "media_type": "movie",
          "genre_ids": [
            18
          ],
          "popularity": 58.349,
          "release_date": "1976-11-21",
          "video": false,
          "vote_average": 7.79,
          "vote_count": 7307
        },
        {
          "adult": false,
          "backdrop_path": "/41QdmNKbwmVD7R6A7hMqMe9FYMy.jpg",
          "id": 76163,
          "title": "The Expendables 2",
          "original_language": "en",
          "original_title": "The Expendables 2",
          "overview": "Mr. Church reunites the Expendables for what should be an easy paycheck, but when one of their men is murdered on the job, their quest for revenge puts them deep in enemy territory and up against an unexpected threat.",
          "poster_path": "/4EBO8aIeP2bF1jGpwbuRS4CFMca.jpg",
          "media_type": "movie",
          "genre_ids": [
            28,
            12,
            53
          ],
          "popularity": 46.023,
          "release_date": "2012-08-08",
          "video": false,
          "vote_average": 6.332,
          "vote_count": 6397
        }
      ]
    }
  ];
  export default people;